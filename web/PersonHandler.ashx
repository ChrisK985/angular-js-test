﻿<%@ WebHandler Language="C#" Class="PersonHandler" %>

using System;
using System.Web;
using Directory;
using System.Reflection;
using globalHelper;
using System.Collections.Specialized;

public class PersonHandler : IHttpHandler
{

    public void ProcessRequest(HttpContext context)
    {
        jsonResponse json = new jsonResponse("200", "SUCCESS");

        try
        {
            switch (context.Request["action"])
            {
                case "search":
                    json = new jsonResponse("200", "SUCCESS", People.search(getPersonDataFromPost(context))); break;
                case "insert":
                    People.insert(getPersonDataFromPost(context)); break;
                case "update":
                    People.update(getPersonDataFromPost(context)); break;
                case "delete":
                    People.delete(Convert.ToInt32(context.Request["id"])); break;
                default:
                    break;
            }
        }
        catch (Exception ex)
        {
            json = new jsonResponse("500", "ERROR: " + ex.Message);
        }
        json.toOutput();

    }

    private Person getPersonDataFromPost(HttpContext context)
    {
        try{ return (Person)jsonSerialization.fromJSON<Person>(context.Request.Form[0]); }
        catch{ return null; }

    }



    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

}