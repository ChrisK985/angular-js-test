﻿var formApp = angular.module('formApp', []);

formApp.controller('formController', function ($scope, $http) {

    $scope.insertPerson = function () {
        $http({
            method: 'POST',
            url: '/PersonHandler.ashx?action=insert',
            data: $scope.formData,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function () {
            $scope.reset();
            $scope.searchPeople();

        });
    };

    $scope.loadPerson = function (u) {
        $scope.formData = u;
        //Get milliseconds of DOB from string.
        $scope.formData.dateofbirth = new Date(parseFloat(u.dateofbirth.replace(/^\D+/g, '')));
        $(".update-person").removeClass("hide");
        $(".insert-person").addClass("hide");
    };

    $scope.updatePerson = function (u) {
        $http({
            method: 'POST',
            url: '/PersonHandler.ashx?action=update',
            data: $scope.formData,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function () {
            $scope.reset();
            $scope.searchPeople();

        });
    };

    $scope.deletePerson = function (_id) {
        $http({
            method: 'POST',
            url: '/PersonHandler.ashx?action=delete',
            params: { id: _id },
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function () {
            $scope.reset();
            $scope.searchPeople();
        });
    };
    
    $scope.searchPeople = function (searchTerm) {
        $http({
            method: 'POST',
            url: '/PersonHandler.ashx?action=search',
            data: $scope.formData,
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
        }).then(function (result) {
            if (result.data) $scope.users = result.data.data;
            $scope.listTitle = ($scope.formData ? "Search Results" : "All People");
        });
    };

    $scope.reset = function () {
        $scope.users = {};
        $scope.formData = {};
        $(".update-person").addClass("hide");
        $(".insert-person").removeClass("hide");
    };

    $scope.searchPeople();
});