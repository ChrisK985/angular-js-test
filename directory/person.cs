﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;

namespace Directory
{
    public class People
    {
        private static string connName = "sqlconnection";
        public static List<Person> search(Person SearchBy = null)
        {

            //try
            //{
                using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connName].ConnectionString))
                using (var cmd = new SqlCommand() { CommandType = CommandType.Text })
                {
                    
                    string query = "SELECT * FROM dir_person";

                    if (SearchBy != null)
                    foreach (var prop in SearchBy.GetType().GetProperties().Where(p => p.GetValue(SearchBy) != null))
                    {
                        query += string.Format(" {0} ({1} LIKE @{1})", (cmd.Parameters.Count == 0 ? " WHERE " : " AND "), prop.Name);
                        cmd.Parameters.AddWithValue("@" + prop.Name, "%" + prop.GetValue(SearchBy).ToString() + "%");
                    }

                    
                    cmd.CommandText = query;
                    cmd.Connection = conn;
                    conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    return loadDataIntoObject(dt); 
                }
            //}
            //catch
            //{
            //    return null;
            //}

        }

        public static void insert(Person p)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connName].ConnectionString))
            using (var cmd = new SqlCommand("directory_person_insert", conn) { CommandType = CommandType.StoredProcedure })
            {
                conn.Open();
                foreach (var prop in p.GetType().GetProperties().Where(pr => pr.Name != "id"))
                {
                    cmd.Parameters.AddWithValue("@" + prop.Name, (prop.GetValue(p) != null ? prop.GetValue(p) : DBNull.Value));
                }

                cmd.ExecuteNonQuery();
            }
        }

        public static void update(Person p)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connName].ConnectionString))
            using (var cmd = new SqlCommand("directory_person_update", conn) { CommandType = CommandType.StoredProcedure })
            {
                conn.Open();
                foreach (var prop in p.GetType().GetProperties())
                {
                    cmd.Parameters.AddWithValue("@" + prop.Name, (prop.GetValue(p) != null ? prop.GetValue(p) : DBNull.Value));
                }

                cmd.ExecuteNonQuery();
            }
        }

        public static void delete(int id)
        {
            using (var conn = new SqlConnection(ConfigurationManager.ConnectionStrings[connName].ConnectionString))
            using (var cmd = new SqlCommand("directory_person_delete", conn) { CommandType = CommandType.StoredProcedure })
            {
                conn.Open();
                cmd.Parameters.Add(new SqlParameter("@id", id));
                cmd.ExecuteNonQuery();
            }
        }

        private static List<Person> loadDataIntoObject(DataTable dt)
        {
            List<Person> list = new List<Person>();
            foreach (var row in dt.AsEnumerable())
            {
                Person obj = new Person();

                foreach (var prop in obj.GetType().GetProperties())
                {
                    try
                    {
                        PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                        propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], (Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType)), null);
                    }
                    catch { continue; }
                }
                list.Add(obj);
            }

            return list;
        }

        
    }

    public class Person
    {
        public int? id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string email { get; set; }
        public DateTime? dateofbirth { get; set; }
        public string telephone { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }


    }

}
