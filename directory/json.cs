﻿using System;
using System.Web;

namespace globalHelper
{
    public class jsonResponse
    {
        public string code { get; set; }
        public string message { get; set; }
        public object data { get; set; }

        public jsonResponse(string xcode, string xmessage, object xdata)
        {
            data = xdata;
            code = xcode;
            message = xmessage;
            
        }

        public jsonResponse(string xcode, string xmessage)
        {
            code = xcode;
            message = xmessage;
        }

        public void toOutput()
        {
            HttpContext.Current.Response.ContentType = "text/javascript";
            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            HttpContext.Current.Response.Write(js.Serialize(this));
        }
    }

    public class jsonSerialization
    {
        public static string toJSON(object obj)
        {
            try
            {
                System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
                js.MaxJsonLength = Int32.MaxValue;

                return js.Serialize(obj);
            }
            catch { }

            return "";
        }

        public static object fromJSON<T>(string source)
        {
            System.Web.Script.Serialization.JavaScriptSerializer js = new System.Web.Script.Serialization.JavaScriptSerializer();
            js.MaxJsonLength = Int32.MaxValue;

            return js.Deserialize<T>(source);
        }
    }
}
